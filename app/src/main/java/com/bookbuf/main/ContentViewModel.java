package com.bookbuf.main;


import android.databinding.BaseObservable;
import android.databinding.Bindable;

/**
 * Created by bo.wei on 2017/2/22.
 */

public class ContentViewModel extends BaseObservable{
    private String title = "hello world";

    @Bindable
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        notifyPropertyChanged(BR.title);
    }
}
